bl_info = {
    "name": "Hair Creator",
    "author": "Your Name Here",
    "version": (1, 0),
    "blender": (2, 83, 4),
    "location": "View3D > UI > Tools > Hair Creator",
    "description": "Addon for creating hair from mesh or curve",
    "warning": "",
    "doc_url": "",
    "category": "Particle",
}

import bpy
import bmesh
from bpy_extras import view3d_utils
from mathutils import Vector, Matrix
from mathutils.geometry import intersect_point_line


def get_guides_from_curve(target, object) :
    depsgraph = bpy.context.evaluated_depsgraph_get()

    object = object.evaluated_get(depsgraph)

    guides = []
    for spline in object.data.splines :

        guide = [object.matrix_world @ Vector(p.co[:3]) for p in spline.points]
        #guide = [target.matrix_world.inverted()@co for co in guide]

        guides.append(guide)

    return guides


def get_guides_from_mesh(target, object) :
    depsgraph = bpy.context.evaluated_depsgraph_get()

    guides = []
    bm = bmesh.new()
    bm.from_object(object, depsgraph)

    object = object.evaluated_get(depsgraph)

    seam_edges = [e for e in bm.edges if e.seam]
    verts = set()
    for e in seam_edges :
        for v in e.verts :
            verts.add(v)

    loop_edges = [e for e in bm.edges if set(e.verts)&verts and e not in seam_edges]

    for e in loop_edges :
        guide = [e]

        loop = e.link_loops[0]

        direction = 'prev'
        if loop.link_loop_prev.edge in seam_edges :
            direction = 'next'

        #print('direction', direction)
        keep_search = True
        while keep_search :

            #next_loop
            if direction == 'prev' :
                loop = loop.link_loop_prev.link_loop_radial_prev.link_loop_prev
            elif direction == 'next' :
                loop = loop.link_loop_next.link_loop_radial_next.link_loop_next

            if loop.edge in guide :
                break

            for v in loop.edge.verts :
                if not guide[0].is_boundary and v.is_boundary :
                    #print('BOUNDARY')
                    keep_search = False
                if guide[0].is_boundary and len(v.link_edges) <=2 :
                    #print('BOUNDARY 2')
                    keep_search = False

            guide.append (loop.edge)

        ## Reorder
        ordered_verts = guide[0].verts[:]
        if ordered_verts[-1] in verts :
            ordered_verts = ordered_verts[::-1]

        for e in guide[1:] :
            ordered_verts.append(e.other_vert(ordered_verts[-1]))

        guide = [object.matrix_world@v.co for v in ordered_verts]
        #guide = [target.matrix_world.inverted()@co for co in guide]

        guides.append(guide)

    bm.free()

    return guides

def get_context() :
    window = bpy.context.window
    screen = bpy.context.screen
    area = next(a for a in screen.areas if a.type=='VIEW_3D')
    region = next(r for r in area.regions if r.type == 'WINDOW')
    return {'window': window, 'screen': screen, 'area': area, 'region': region}

'''
def _set_camera(context, target, face_index) :

    #bpy.ops.object.mode_set(mode='EDIT')
    bm = bmesh.from_edit_mesh(target.data)
    bm.faces.ensure_lookup_table()

    bm.faces[face_index].select = True
    bmesh.update_edit_mesh(target.data)

    #bpy.ops.view3d.view_persportho(context)

    bpy.ops.view3d.view_axis(context, type="TOP", align_active=True)
    bpy.ops.view3d.view_selected(context)

    bm.faces[face_index].select = False

    bpy.ops.object.mode_set(mode='OBJECT')

    context['area'].spaces[0].region_3d.update()

    print(context['area'].spaces[0].region_3d.view_matrix )
    '''


def get_hair_stokes_data(context, target, guides) :
    depsgraph = bpy.context.evaluated_depsgraph_get()
    target = target.evaluated_get(depsgraph)

    space_data = context['area'].spaces[0]
    rv3d = space_data.region_3d
    region = context['region']

    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action='DESELECT')
    bpy.ops.object.mode_set(mode='OBJECT')

    faces_data = []
    for guide in guides :

        root = guide[0]

        (hit, loc, norm, face_index) = target.closest_point_on_mesh(root)

        face = target.data.polygons[face_index]
        vector = (face.center - loc).normalized()

        pt = target.matrix_world @ loc #(loc+vector*0.15) # maybe add offset in pixels

        faces_data.append({'hair_keys': guide, 'point': pt, 'face_index': face_index})

    bpy.ops.object.mode_set(mode='EDIT')
    bm = bmesh.from_edit_mesh(target.data)
    bm.faces.ensure_lookup_table()

    hair_stokes_data = []
    indexes = set(map(lambda x:x['face_index'], faces_data))
    for index in indexes :
        face = bm.faces[index]

        face.select = True
        bmesh.update_edit_mesh(target.data)

        # Zoom to face
        bpy.ops.view3d.view_axis(context, type="TOP", align_active=True)
        rv3d.view_perspective = 'PERSP'

        bpy.ops.view3d.view_selected(context)

        #raise Exception('Failed to create particle')

        face.select = False

        rv3d.update()



        # Distance to camera
        world_verts = [v.co@target.matrix_world for v in face.verts]

        view_matrix = rv3d.view_matrix.copy()
        view_center = view_matrix.inverted().to_translation()
        view_end = view_matrix.inverted()@Vector((0,0,-space_data.clip_end))

        vectors = [intersect_point_line(v, view_center, view_end)[0] for v in world_verts]
        distances = [(v-view_center).length for v in vectors]

        hair_stoke_data = {'view_matrix' : view_matrix, 'strokes_data' : [],
        'clip_start': min(distances)-0.0015 }

        for face_data in [x for x in faces_data if x['face_index']==index] :
            # Add view_matrix and get mouse pos
            mouse = view3d_utils.location_3d_to_region_2d(region, rv3d, face_data['point'], default=None)

            hair_stoke_data['strokes_data'].append({'mouse': mouse,
            'hair_keys' : face_data['hair_keys'], 'face_index': face_data['face_index']})

        hair_stokes_data.append(hair_stoke_data)

    return hair_stokes_data

def add_hair(context, target, face_index, hair_keys, mouse, particle_system=None, sample=None):
    scene = bpy.context.scene

    #depsgraph = bpy.context.evaluated_depsgraph_get()
    #eval_target = target.evaluated_get(depsgraph)
    #target = eval_target.original

    #particle_system = particle_system or eval_target.particle_systems.active

    #hair_keys = [eval_target.matrix_world.inverted()@co for co in hair_keys]

    #root = hair_keys[0]
    key_count = len(hair_keys)

    #scene.cursor.location = hair_keys[-1]

    face = target.data.polygons[face_index]

    '''
    if target.vertex_groups.get('_face') :
        target.vertex_groups.remove(target.vertex_groups['_face'])

    vg = target.vertex_groups.new(name='_face')
    vg.add([v for v in face.vertices], 1, 'ADD')

    #print(vg)

    if target.modifiers.get('_face') :
        target.modifiers.remove(target.modifiers['_face'])

    ## Mask modifier
    m = target.modifiers.new('_face', 'MASK')
    m.vertex_group = vg.name
    for i in range(len(target.modifiers)-1) :
        bpy.ops.object.modifier_move_up(modifier=m.name)

    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.object.mode_set(mode='OBJECT')

    bpy.context.view_layer.update()
    '''


    depsgraph = bpy.context.evaluated_depsgraph_get()

    eval_target = target.evaluated_get(depsgraph)
    particle_system = eval_target.particle_systems.active
    particles_nb = len(particle_system.particles)

    #bpy.ops.particle.particle_edit_toggle()

    bpy.ops.particle.particle_edit_toggle()
    bpy.ops.particle.select_all(action='SELECT')
    bpy.ops.particle.hide()

    bpy.ops.wm.tool_set_by_id(context, name="builtin_brush.Add")

    scene.tool_settings.particle_edit.default_key_count = key_count
    scene.tool_settings.particle_edit.brush.count = 1

    bpy.ops.particle.brush_edit(context, stroke=[{"name":"", "location":(0, 0, 0),
    "mouse":mouse, "pressure":0, "size":0, "pen_flip":False, "time":0, "is_start":False} ])


    bpy.ops.particle.select_all(action='SELECT')
    bpy.ops.particle.particle_edit_toggle()

    #if target.vertex_groups.get('_face') :
    #    target.vertex_groups.remove(target.vertex_groups['_face'])

    #if target.modifiers.get('_face') :
    #    target.modifiers.remove(target.modifiers['_face'])



    #

    #rv3d = context['area'].spaces[0].region_3d
    #region = context['region']

    #print('\n >>>>>> Mouse', mouse)
    #scene.cursor.location = view3d_utils.region_2d_to_location_3d(region ,rv3d, mouse, depth_location=Vector((0.0, 0.0, 1.0)))

    #bpy.ops.particle.particle_edit_toggle()

    #print('\n >>>>> Mode:', target.mode)
    #bpy.ops.particle.particle_edit_toggle()
    #bpy.ops.particle.particle_edit_toggle()

    depsgraph = bpy.context.evaluated_depsgraph_get()

    eval_target = target.evaluated_get(depsgraph)
    particle_system = eval_target.particle_systems.active

    if len(particle_system.particles) == particles_nb :
        print('Failed to create particle')
        #raise Exception('Failed to create particle')
        #bpy.ops.particle.particle_edit_toggle()
        #bpy.ops.particle.particle_edit_toggle()
        return

    particle = particle_system.particles[-1]

    #print('\n>>>> Particle as a len of', len(particle.hair_keys))
    local_hair_keys = [eval_target.matrix_world.inverted()@co for co in hair_keys]

    #particle.prev_location = hair_keys[0]
    #particle.location = local_hair_keys[0]
    #p.prev_location = guide[0]
    #for i in range(1, len(particle.hair_keys)) :
    #    particle.hair_keys[i].co = local_hair_keys[i]

    flat_co = [v for co in local_hair_keys for v in co]
    particle.hair_keys.foreach_set('co', flat_co)

    bpy.ops.particle.particle_edit_toggle()
    bpy.ops.particle.particle_edit_toggle()

    #bpy.ops.particle.particle_edit_toggle()

    #bpy.ops.particle.particle_edit_toggle()
    #bpy.ops.particle.particle_edit_toggle()


    if sample :
        bpy.ops.particle.particle_edit_toggle()

        hair_len = sum([(hair_keys[i] - hair_keys[i-1]).length for i in range(1, key_count)])
        bpy.ops.particle.rekey(keys_number=round(hair_len/sample) )
        bpy.ops.particle.particle_edit_toggle()


    return particle

class HG_OT_create_hair(bpy.types.Operator):
    bl_idname = "object.create_hair"
    bl_label = "Copy Vert IDs"
    bl_description = "Copy verts IDs by topology \
    (you need to selected two faces)\nMesh shape can be different, bu topology must be the same"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context) :
        depsgraph = context.evaluated_depsgraph_get()
        scene = context.scene

        props = scene.hair_creator

        target = context.object
        eval_target = target.evaluated_get(depsgraph)
        selected_objects = [o for o in context.selected_objects if o is not target]
        #target = ob.evaluated_get(depsgraph)

        override = get_context()

        # Store View info for restoring everything at the end
        space_data = override['area'].spaces[0]
        rv3d = space_data.region_3d
        mode = target.mode

        view_matrix = rv3d.view_matrix.copy()
        view_perspective = rv3d.view_perspective
        view_distance = rv3d.view_distance

        clip_start = space_data.clip_start
        #clip_end = space_data.clip_end
        lens = space_data.lens

        #space_data.lens = 250

        guides = []
        for ob in selected_objects :
            if ob.type == 'MESH' :
                guides +=  get_guides_from_mesh(target, ob)

            elif ob.type == 'CURVE' :
                guides += get_guides_from_curve(target, ob)

        #print('\n >>>>> guides', guides)
        hair_stokes_data = get_hair_stokes_data(override, target, guides)

        #print('\n >>>>> hair_stokes_data', hair_stokes_data)

        # Set Up Particle system
        bpy.ops.particle.edited_clear()

        particle_system = target.particle_systems.active
        particle_settings = particle_system.settings

        particle_settings.emit_from = 'FACE'
        particle_settings.use_emit_random = False
        particle_settings.count = 0
        scene.tool_settings.particle_edit.use_emitter_deflect = False
        scene.tool_settings.particle_edit.use_preserve_length = False

        # Set particle system as edited
        bpy.ops.particle.particle_edit_toggle()
        scene.tool_settings.particle_edit.tool = 'COMB'
        bpy.ops.transform.translate()
        bpy.ops.particle.particle_edit_toggle()


        for hair_stroke_data in hair_stokes_data :
            # Set the view before adding hair
            rv3d.view_matrix = hair_stroke_data['view_matrix']
            #space_data.clip_start = hair_stroke_data['clip_start']
            space_data.clip_start = hair_stroke_data['clip_start']

            rv3d.update()

            #raise Exception('Debug')

            for hair_stroke in hair_stroke_data['strokes_data'] :
                p = add_hair(override, target,
                face_index = hair_stroke['face_index'],
                hair_keys = hair_stroke['hair_keys'],
                mouse = hair_stroke['mouse'],
                sample=props.sample)

        rv3d.view_perspective = view_perspective
        rv3d.view_distance = view_distance
        rv3d.view_matrix= view_matrix

        space_data.clip_start = clip_start
        #space_data.clip_end = clip_end
        space_data.lens = lens

        rv3d.update()

        # Make shure particle are updated
        bpy.ops.particle.particle_edit_toggle()
        bpy.ops.particle.particle_edit_toggle()

        return {'FINISHED'}


class HG_OT_resample_hair(bpy.types.Operator):
    bl_idname = "object.resample_hair"
    bl_label = "Copy Vert IDs"
    bl_description = "Copy verts IDs by topology \
    (you need to selected two faces)\nMesh shape can be different, bu topology must be the same"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context) :
        print('resample_hair')
        return {'FINISHED'}


class VIEW3D_PT_hair_creator(bpy.types.Panel):
    bl_label = "Hair Creator"
    bl_category = "Tools"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'

    def draw(self, context):
        props = context.scene.hair_creator
        layout = self.layout
        row = layout.row(align= True)
        row.prop(props, "sample", text='Sample')
        row = layout.row(align= True)
        row.operator("object.create_hair",icon="HAIR",text ='Create Hair' )
        row = layout.row(align= True)
        row.operator("object.resample_hair",icon="MESH_DATA",text = 'Resample Hair')


class HGProps(bpy.types.PropertyGroup) :
    sample : bpy.props.FloatProperty()

classes = (
HGProps,
HG_OT_create_hair,
HG_OT_resample_hair,
VIEW3D_PT_hair_creator
)

def register():
    for cls in classes :
        bpy.utils.register_class(cls)
    bpy.types.Scene.hair_creator = bpy.props.PointerProperty(type= HGProps)

def unregister():
    del bpy.types.Scene.hair_creator
    for cls in classes[::-1] :
        bpy.utils.unregister_class(cls)
